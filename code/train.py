import argparse
import json
import numpy as np
import os
import pandas as pd
import sys
import time
import torch.distributed as dist
from pathlib import Path
from torch.utils.data import DistributedSampler
from tqdm import tqdm

from dataset import *
from metrics import *
from model import *

os.environ['CUDA_VISIBLE_DEVICES'] = "0, 1"

# parser = argparse.ArgumentParser(description='Process some arguments')
# parser.add_argument('--model_name_or_path', type=str, default='microsoft/codebert-base')
# parser.add_argument('--train_mark_path', type=str, default=)
# parser.add_argument('--train_features_path', type=str, default=)
# parser.add_argument('--val_mark_path', type=str, default=)
# parser.add_argument('--val_features_path', type=str, default=)
# parser.add_argument('--val_path', type=str, default=)
#
# parser.add_argument('--md_max_len', type=int, default=64)
# parser.add_argument('--total_max_len', type=int, default=512)
# parser.add_argument('--batch_size', type=int, default=8)
# parser.add_argument('--accumulation_steps', type=int, default=4)
# parser.add_argument('--epochs', type=int, default=5)
# parser.add_argument('--n_workers', type=int, default=8)
#
# args = parser.parse_args()
# os.mkdir("./outputs")
data_dir = Path('/home/khanhpluto/AI4Code/AI4Code')

train_df_mark = pd.read_csv('/home/khanhpluto/AI4Code/train_mark.csv').drop(
    "parent_id", axis=1).dropna().reset_index(drop=True)
train_fts = json.load(open('/home/khanhpluto/AI4Code/train_fts.json'))
val_df_mark = pd.read_csv('/home/khanhpluto/AI4Code/val_mark.csv').drop(
    "parent_id", axis=1).dropna().reset_index(drop=True)
val_fts = json.load(open('/home/khanhpluto/AI4Code/val_fts.json'))
val_df = pd.read_csv("/home/khanhpluto/AI4Code/val.csv")

order_df = pd.read_csv("/home/khanhpluto/AI4Code/AI4Code/train_orders.csv").set_index("id")
df_orders = pd.read_csv(
    data_dir / 'train_orders.csv',
    index_col='id',
    squeeze=True,
).str.split()


def init_distributed():
    # Initializes the distributed backend which will take care of synchronizing nodes/GPUs
    dist_url = "env://"  # default

    # only works with torch.distributed.launch // torch.run
    rank = int(os.environ["RANK"])
    world_size = int(os.environ['WORLD_SIZE'])
    local_rank = int(os.environ['LOCAL_RANK'])
    dist.init_process_group(
        backend="nccl",
        init_method=dist_url,
        world_size=world_size,
        rank=rank)

    # this will make all .cuda() calls work properly
    torch.cuda.set_device(local_rank)

    # synchronizes all the threads to reach this point before moving on
    dist.barrier()


def read_data(data):
    return tuple(d.cuda() for d in data[:-1]), data[-1].cuda()


def validate(model, val_loader):
    model.eval()

    tbar = tqdm(val_loader, file=sys.stdout)

    preds = []
    labels = []

    with torch.no_grad():
        for idx, data in enumerate(tbar):
            inputs, target = read_data(data)

            with torch.cuda.amp.autocast():
                pred = model(*inputs)

            preds.append(pred.detach().cpu().numpy().ravel())
            labels.append(target.detach().cpu().numpy().ravel())

    return np.concatenate(labels), np.concatenate(preds)


def is_dist_avail_and_initialized():
    if not dist.is_available():
        return False

    if not dist.is_initialized():
        return False

    return True


def get_rank():
    if not is_dist_avail_and_initialized():
        return 0

    return dist.get_rank()


def is_main_process():
    return get_rank() == 0


def train(model, train_loader, val_loader, epochs):
    np.random.seed(0)
    # Creating optimizer and lr schedulers
    param_optimizer = list(model.named_parameters())
    no_decay = ['bias', 'LayerNorm.bias', 'LayerNorm.weight']
    optimizer_grouped_parameters = [
        {'params': [p for n, p in param_optimizer if not any(nd in n for nd in no_decay)], 'weight_decay': 0.01},
        {'params': [p for n, p in param_optimizer if any(nd in n for nd in no_decay)], 'weight_decay': 0.0}
    ]

    num_train_optimization_steps = int(5 * len(train_loader) / 4)
    optimizer = AdamW(optimizer_grouped_parameters, lr=3e-5,
                      correct_bias=False)  # To reproduce BertAdam specific behavior set correct_bias=False
    scheduler = get_linear_schedule_with_warmup(optimizer, num_warmup_steps=0.05 * num_train_optimization_steps,
                                                num_training_steps=num_train_optimization_steps)  # PyTorch scheduler

    criterion = torch.nn.L1Loss()
    scaler = torch.cuda.amp.GradScaler()
    y_pred = 0

    for e in range(epochs):
        model.train()
        train_loader.sampler.set_epoch(e)
        tbar = tqdm(train_loader, file=sys.stdout)
        loss_list = []
        preds = []
        labels = []

        for idx, data in enumerate(tbar):
            inputs, target = read_data(data)

            with torch.cuda.amp.autocast():
                pred = model(*inputs)
                loss = criterion(pred, target)
            scaler.scale(loss).backward()
            if idx % 4 == 0 or idx == len(tbar) - 1:
                scaler.step(optimizer)
                scaler.update()
                optimizer.zero_grad()
                scheduler.step()

            loss_list.append(loss.detach().cpu().item())
            preds.append(pred.detach().cpu().numpy().ravel())
            labels.append(target.detach().cpu().numpy().ravel())

            avg_loss = np.round(np.mean(loss_list), 4)

            tbar.set_description(f"Epoch {e + 1} Loss: {avg_loss} lr: {scheduler.get_last_lr()}")
        if is_main_process():
            y_val, y_pred = validate(model, val_loader)
            val_df["pred"] = val_df.groupby(["id", "cell_type"])["rank"].rank(pct=True)
            val_df.loc[val_df["cell_type"] == "markdown", "pred"] = y_pred
            y_dummy = val_df.sort_values("pred").groupby('id')['cell_id'].apply(list)
            print("Preds score", kendall_tau(df_orders.loc[y_dummy.index], y_dummy))
            torch.save(model.state_dict(), f"./outputs/model_{e}.bin")

    return model, y_pred


init_distributed()
train_ds = MarkdownDataset(train_df_mark, model_name_or_path='microsoft/codebert-base', md_max_len=64,
                           total_max_len=512, fts=train_fts)
val_ds = MarkdownDataset(val_df_mark, model_name_or_path='microsoft/codebert-base', md_max_len=64,
                         total_max_len=512, fts=val_fts)
# train_loader = DataLoader(train_ds, batch_size=8, shuffle=True, num_workers=8,
#                           pin_memory=False, drop_last=True)
train_sampler = DistributedSampler(dataset=train_ds, shuffle=True)
train_loader = torch.utils.data.DataLoader(train_ds, batch_size=8,
                                           sampler=train_sampler, num_workers=4, pin_memory=True)

# val_loader = DataLoader(val_ds, batch_size=8, shuffle=False, num_workers=8,
#                         pin_memory=False, drop_last=False)
val_sampler = DistributedSampler(dataset=val_ds, shuffle=True)
val_loader = torch.utils.data.DataLoader(val_ds, batch_size=8,
                                         sampler=val_sampler, num_workers=4, pin_memory=True)
model = MarkdownModel('microsoft/codebert-base')
ckpt_path = '/home/khanhpluto/AI4Code/outputs/model.bin'
model.load_state_dict(torch.load(ckpt_path))
model = nn.SyncBatchNorm.convert_sync_batchnorm(model)
local_rank = int(os.environ['LOCAL_RANK'])
model = model.cuda()
model = nn.parallel.DistributedDataParallel(model, device_ids=[local_rank], find_unused_parameters=True)
start_train = time.time()
model, y_prediction = train(model, train_loader, val_loader, epochs=10)
end_train = time.time()
